import java.util.Scanner;

public class CustomerInfo {
    public static void main(String[] args){
        Scanner read = new Scanner(System.in);
        String firstName = read.nextLine();
        String lastName = read.nextLine();
        int age = read.nextInt();
        int roomNumber = read.nextInt();
        Customer customer = new Customer();
        //Set customer's data to object here
        customer.firstName = firstName;
        customer.lastName = lastName;
        customer.age = age;
        customer.roomNumber = roomNumber;

        customer.saveCustomerInfo();
    }
}

class Customer {
    //Add all necessary attributes here
    String firstName;
    String lastName;
    int age;
    int roomNumber;
    public void saveCustomerInfo() {
        System.out.println("First Name: " + firstName);
        System.out.println("Last Name " + lastName);
        System.out.println("Age " + age);
        System.out.println("Room number " + roomNumber);
    }
}